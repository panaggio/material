---
track: Hardware
title: Comunicação entre componentes
---

A comunicação é feita via uma API.

Na ilustração abaixo,
os retângulos cinza claro correspondem a uma camada de software,
os octógonos à implementação da camada,
e os retângulos azuis à API que interfaceia duas implementações.

![Ilustração da pilha de software](/img/hardware/stack.svg)

É possível existir mais de uma implementação para uma mesma camada,
por exemplo, existe o kernel Linux, o kernel BSD, etc.
Além disso,
a implementação de uma camada pode estar dividida em vários programas,
por exemplo, a camada de aplicativos do usuário é composta de vários programas.