---
track: Hardware
title: Ligando um LED
---

Nessa seção você vai ligar um LED utilizando [Arduino](arduino.cc),
um hardware de código aberto,
e [Scratch](http://scratch.mit.edu/),
uma linguagem de programação que será vista em maiores detalhes
nas seções seguintes.
