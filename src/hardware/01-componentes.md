---
track: Hardware
title: Componentes do hardware
---

<figure class="columns4">
<a title="By The original uploader was Muband at Japanese Wikipedia (Transferred from ja.wikipedia to Commons.) [CC-BY-SA-3.0 (http://creativecommons.org/licenses/by-sa/3.0/)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ASuper_Nintendo_Entertainment_System-USA.jpg"><img alt="Video-game" src="//upload.wikimedia.org/wikipedia/commons/2/24/Super_Nintendo_Entertainment_System-USA.jpg"/></a>
<a title="J-P Kärnä [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC-BY-SA-3.0 (http://creativecommons.org/licenses/by-sa/3.0/)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ACanon_Digital_Ixus_430.jpg"><img alt="Câmera digital" src="//upload.wikimedia.org/wikipedia/commons/1/11/Canon_Digital_Ixus_430.jpg"/></a>
<a title="By Firefoxman (Own work) [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC BY-SA 4.0-3.0-2.5-2.0-1.0 (http://creativecommons.org/licenses/by-sa/4.0-3.0-2.5-2.0-1.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ADell_XPS_M140.jpg"><img alt="Notebook" src="//upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Dell_XPS_M140.jpg/512px-Dell_XPS_M140.jpg"/></a>
<a title="By LG전자 [CC BY 2.0 (http://creativecommons.org/licenses/by/2.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ALG_G_Pro_2_(White).jpg"><img alt="Smartphone" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/ff/LG_G_Pro_2_%28White%29.jpg/256px-LG_G_Pro_2_%28White%29.jpg"/></a>
<figcaption>Figura: Diferentes dispositivos eletrônicos.</figcaption>
</figure>

O que os dispositivos acima possuem em comum?
Um computador ou dispositivo eletrônico semelhante,
como video-game, celular e tablet,
possui **obrigatoriamente** os seguintes componentes:

-   processador,
-   memória,
-   dispositivo de armazenamento,
-   dispositivo de entrada, e
-   dispositivo de saída.

### Processador

<figure class="columns4">
<a title="By Viswesr (Own work) [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AEFM32TG_MCUs.jpg"><img alt="Micro-processador" src="//upload.wikimedia.org/wikipedia/commons/8/84/EFM32TG_MCUs.jpg"/></a>
<a title="By Jean-Pierre [CC BY-SA 2.0 (http://creativecommons.org/licenses/by-sa/2.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AIntel_Pentium_II_Klamath_processor_-_inside_view_A.jpg"><img alt="Intel Pentium II" src="//upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Intel_Pentium_II_Klamath_processor_-_inside_view_A.jpg/512px-Intel_Pentium_II_Klamath_processor_-_inside_view_A.jpg"/></a>
<a title="By KB Alpha (Own work) [CC BY 3.0 (http://creativecommons.org/licenses/by/3.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AHT-Pentium4.JPG"><img alt="Intel Pentium4" src="//upload.wikimedia.org/wikipedia/commons/thumb/9/9c/HT-Pentium4.JPG/512px-HT-Pentium4.JPG"/></a>
<a title="By Evan-Amos (Own work) [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ASapphire-Radeon-HD-5570-Video-Card.jpg"><img alt="Placa de Vídeo" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Sapphire-Radeon-HD-5570-Video-Card.jpg/512px-Sapphire-Radeon-HD-5570-Video-Card.jpg"/></a>
<figcaption>Figura: Diferentes processadores.</figcaption>
</figure>

O processador ou CPU é quem controla os demais componentes
e faz isso através de longas sequências compostas apenas de zeros e uns.

A principal forma de medir a velocidade de um processador
é pelo número de operações que eles faz por segundo, dada em Hertz:

<table id="tabela-hertz">
<caption>Tabela: Múltiplos de Hertz</caption>
<tr>
<th>Nome</th>
<th>Abreviação</th>
<th>Valor</th>
<th>Onde é encontrado</th>
</tr>
<tr>
<td>Mega Hertz</td>
<td>MHz</td>
<td>$10^6$ de operações por segundo</td>
<td>Microcontroladores</td>
</tr>
<tr>
<td>Giga Hertz</td>
<td>GHz</td>
<td>$10^9$ de operações por segundo</td>
<td>PCs, notebooks, tablets e celulares</td>
</tr>
</table>

<aside>
#### Aquecimento

O processador aquece durante seu funcionamento
e se atingir temperaturas muito elevadas ele pode "derreter".
Para evitar o derretimento muitos processadores precisam de um dissipador.

<figure class="columns4">
<a title="By Sebastian high (Own work) [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AModerate-Pin-Fin-Heatsink-Aluminium-.jpg"><img alt="Dissipador para micro-processadores" src="//upload.wikimedia.org/wikipedia/commons/8/8b/Moderate-Pin-Fin-Heatsink-Aluminium-.jpg"/></a>
<a title="By Mboverload~commonswiki [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:N64-cartridge-chip_side-wo_heatsink.jpg"><img alt="Dissipador em cartucho de video-game" src="//upload.wikimedia.org/wikipedia/commons/thumb/d/d4/N64-cartridge-chip_side-wo_heatsink.jpg/667px-N64-cartridge-chip_side-wo_heatsink.jpg"/></a>
<a title="By Medjaï (Own work) [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ACooler_Master_V8.jpg"><img alt="Dissipador para PC" src="//upload.wikimedia.org/wikipedia/commons/thumb/2/27/Cooler_Master_V8.jpg/512px-Cooler_Master_V8.jpg"/></a>
<a title="By Gary Houston (Own work) [CC0], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AAsus-GeForce-GT730-20141112-001.jpg"><img alt="Dissipador em placa de vídeo" src="//upload.wikimedia.org/wikipedia/commons/thumb/3/39/Asus-GeForce-GT730-20141112-001.jpg/512px-Asus-GeForce-GT730-20141112-001.jpg"/></a>
<figcaption>Figura: Dissipadores para diferentes processadores.</figcaption>
</figure>
</aside>

<aside>
#### Processador em periféricos

Muitos periféricos possuem seu próprio processador.
</aside>

### Memória RAM

<figure class="columns4">
<a title="By R66murull1 (Own work by the original uploader) [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ADDR1.JPG"><img alt="DDR1" src="//upload.wikimedia.org/wikipedia/commons/thumb/6/65/DDR1.JPG/512px-DDR1.JPG"/></a>
<a title="By Appaloosa 11:45, 10 August 2006 (UTC) (taken by Appaloosa) [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC-BY-SA-3.0 (http://creativecommons.org/licenses/by-sa/3.0/)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ADRAM_DDR2_512.jpg"><img alt="DRAM" src="//upload.wikimedia.org/wikipedia/commons/thumb/5/56/DRAM_DDR2_512.jpg/512px-DRAM_DDR2_512.jpg"/></a>
<a title="By Tobias b köhler (Own work) [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3A4GB_DDR3_SO-DIMM.jpg"><img alt="DDR3 SO-DIMM" src="//upload.wikimedia.org/wikipedia/commons/thumb/b/be/4GB_DDR3_SO-DIMM.jpg/512px-4GB_DDR3_SO-DIMM.jpg"/></a>
<a title="By smial (Own work) [FAL or GFDL 1.2 (http://www.gnu.org/licenses/old-licenses/fdl-1.2.html)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ADIMM_DDR3_1600_low_voltage_IMGP6412_wp.jpg"><img alt="DDR3" src="//upload.wikimedia.org/wikipedia/commons/thumb/3/3a/DIMM_DDR3_1600_low_voltage_IMGP6412_wp.jpg/512px-DIMM_DDR3_1600_low_voltage_IMGP6412_wp.jpg"/></a>
<figcaption>Figura: Diferentes memórias RAM.</figcaption>
</figure>

Memória RAM ou volátil é onde o fica armazenado a informação que o processador irá utilizar
como, por exemplo,

-   programa sendo executado,
-   arquivos sendo lidos ou gravados, e
-   imagem que está sendo exibida na tela.

A capacidade de memória de um computador é medida em bytes,
isso é, uma caixa que pode armazenar 8 zeros ou uns.
Como bytes é uma unidade muito pequena
costuma-se a utilizar um múltiplo dela.

<table>
<caption>Tabela: Múltiplos de byte</caption>
<tr>
<th>Nome</th>
<th>Abreviação</th>
<th>Valor</th>
<th>Onde é encontrado</th>
</tr>
<tr>
<td>Kilo byte</td>
<td>KB</td>
<td>$2^{10}$</td>
<td>Microcontroladores</td>
</tr>
<tr>
<td>Mega byte</td>
<td>MB</td>
<td>$(2^{10})^2$</td>
<td>PC (na década de 90)</td>
</tr>
<tr>
<td>Giga byte</td>
<td>GB</td>
<td>$(2^{10})^3$</td>
<td>PC e notebooks</td>
</tr>
<tr>
<td>Tera byte</td>
<td>TB</td>
<td>$(2^{10})^4$</td>
<td>Servidores</td>
</tr>
</table>

A velocidade com que a informação na memória RAM é alterada
é medida em Hertz,
a mesma unidade utilizada para medir a velocidade do processador.

### Dispositivo de armazenamento

<figure class="columns4">
<a title="By Liftarn [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons" href="https://en.wikipedia.org/wiki/File:BESKmemories.jpg"><img alt="Drum memory" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/f9/BESKmemories.jpg/800px-BESKmemories.jpg"/></a>
<a title="By Konstantin Lanzet (received per EMail Camera: Canon EOS 400D) [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC-BY-SA-3.0 (http://creativecommons.org/licenses/by-sa/3.0/)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AKL_CoreMemory.jpg"><img alt="CoreMemory" src="//upload.wikimedia.org/wikipedia/commons/thumb/d/da/KL_CoreMemory.jpg/512px-KL_CoreMemory.jpg"/></a>
<a title="By Flickr upload bot [CC BY 2.0 (http://creativecommons.org/licenses/by/2.0)], via Wikimedia Commons" href="https://en.wikipedia.org/wiki/File:Intel_X25-M_Solid-State_Drive.jpg"><img alt="SSD" src="//upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Intel_X25-M_Solid-State_Drive.jpg/800px-Intel_X25-M_Solid-State_Drive.jpg"/></a>
<a title="By TonyTheTiger CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons" href="https://en.wikipedia.org/wiki/File:External_hard_drives.jpg"><img alt="HD externo" src="//upload.wikimedia.org/wikipedia/commons/thumb/3/3e/External_hard_drives.jpg/800px-External_hard_drives.jpg"/></a>
<figcaption>Figura: Diferentes dispositivos de armazenamento.</figcaption>
</figure>

Dispositivo de armazenamento, memória persistente ou memória não volátil
é onde fica armazenado toda a informação que precisa ser preservada
após o dispositivo eletrônico ser desligado,
por exemplo,

-   sistema operacional,
-   aplicativos de usuário, e
-   arquivos de usuário.

A capacidade de armazenamento é medida em bytes,
assim com a capacidade da memória RAM,
e a velocidade é medida em Hertz,
assim como a velocidade do processador.

<aside>
#### Disco Rígido vs Memória Flash

Em PCs e notebooks o dispositivo de armazenamento mais utilizado
são discos rígidos, sendo (em 2015) os mais populares em 2015 os de 500GB.

Em alguns casos,
os discos rígidos estão sendo substituídos por memória flash
ou discos de estado sólido (SSDs)
que operam a uma velocidade maior que os discos rígidos
mas são bem mais caros.
</aside>

### Dispositivo de entrada e saída

Um computador só é útil se você conseguir interagir com ele
e para isso ele precisa de dispositivos para entrada e saída (E/S).

<figure class="columns4">
<a title="By ArnoldReinhold [Public domain], via Wikimedia Commons" href="https://en.wikipedia.org/wiki/File:Blue-punch-card-front-horiz.png"><img alt="Cartão perfurado" src="//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Blue-punch-card-front-horiz.png/800px-Blue-punch-card-front-horiz.png"/></a>
<a title="By Mboverload~commonswiki [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:N64-controller-blue.jpg"><img alt="Controle do video game Nintendo 64" src="//upload.wikimedia.org/wikipedia/commons/8/86/N64-controller-blue.jpg"/></a>
<a title="By Darkone (Own work) [CC BY-SA 2.5 (http://creativecommons.org/licenses/by-sa/2.5)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3A3-Tastenmaus_Microsoft.jpg"><img alt="Mouse" src="//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/3-Tastenmaus_Microsoft.jpg/512px-3-Tastenmaus_Microsoft.jpg"/></a>
<a title="By Evan-Amos [Public domain], via Wikimedia Commons" href="https://en.wikipedia.org/wiki/File:Xbox-360-Kinect-Standalone.png"><img alt="Kinect" src="//upload.wikimedia.org/wikipedia/commons/thumb/6/67/Xbox-360-Kinect-Standalone.png/799px-Xbox-360-Kinect-Standalone.png"/></a>
<figcaption>Figura: Diferentes dispositivos de entrada.</figcaption>
</figure>

Uma das primeiras formas de passar uma informação para o computador
foi via cartão perfurado (que hoje é peça de museu).
Atualmente os dispositivos de entrada mais comuns são:

-   teclado (ou controle remote),
-   mouse ou touchpad,
-   telas sensíveis ao toque,
-   microfones, e
-   movimento corporal.

<figure class="columns4">
<a title="By Nakamura2828 at us.wikipedia [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC-BY-SA-3.0 (http://creativecommons.org/licenses/by-sa/3.0/)], from Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AEpson_MX-80.jpg"><img alt="Impressora" src="//upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Epson_MX-80.jpg/512px-Epson_MX-80.jpg"/></a>
<a title="By Baran Ivo (Own work) [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ALCD_MAG.jpg"><img alt="Monitor LCD" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/00/LCD_MAG.jpg/512px-LCD_MAG.jpg"/></a>
<a title="By Arpingstone [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ALoudspeaker.arp.500pix.jpg"><img alt="Caixa de som" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/06/Loudspeaker.arp.500pix.jpg/256px-Loudspeaker.arp.500pix.jpg"/></a>
<a title="Ixitixel [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC-BY-SA-3.0 (http://creativecommons.org/licenses/by-sa/3.0/)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Refreshable_Braille_display.jpg"><img alt="Monitor braile" src="//upload.wikimedia.org/wikipedia/commons/0/06/Refreshable_Braille_display.jpg"/></a>
<figcaption>Figura: Diferentes dispositivos saída.</figcaption>
</figure>

Uma das primeiras formas do computador passar uma informação para o usuário
foi através de uma impressora (que não é mais o dispositivo de saída padrão).
Atualmente os dispositivo de saída mais comuns são:

-   monitor e
-   caixa de som.
