---
title: Bem-vindo!!!
---

Olá jovem hacker.
Esse material foi escrito
para você consultá-lo em casa
de forma que possa revisar o que foi visto em sala.

Antes de iniciar o conteúdo técnico,
algumas informações.

### Hackers

> "Olá, eu sou Jacob, e eu sou um programador mediocre."

A frase acima foi proferida por
[Jacob Kaplan-Moss na PyCon2015](https://www.youtube.com/watch?v=hIJdFxYlEKE)
durante sua palestra na qual aborda o mito de que você
só pode programar se você for um "rock star" ou um ninja.
Esse mito está relacionado com um outro mito,
o de que você nasce com **ou sem** o talento para programar.

**Na verdade**,
programação é uma habilidade
como correr, ler, escrever e várias outras
que você pode aprender.

Da mesma forma,
hacker não é um talento mas uma habilidade
muitas vezes associado com

-   ["procurar e explorar falhas em um computador ou uma rede de computadores"](https://en.wikipedia.org/wiki/Hacker_%28computer_security%29),
-   ["modificar software ou hardware de um computador pessoal"](https://en.wikipedia.org/wiki/Hacker_%28hobbyist%29), e
-   ["contornar limitações de forma criativa"](https://en.wikipedia.org/wiki/Hacker_%28programmer_subculture%29).

### Exemplos

Ao final do curso,
algumas coisas que você será capaz de fazer:

-   criar seus próprios jogos.
