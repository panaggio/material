---
track: Scratch
title: Paralelismo
---

## Exemplos

-   [Gato e cachorro atravessando a tela](/instrutores-scratch/exemplos/paralelismo.sb)

## Computação desplugada

Sem sugestão.

## Comentários

Antes de começar a abordar esse tema
é sugerido mencionar que os computadores não trabalham em paralelo.
Os computadores mudam rapidamente de tarefas
de forma que o usuário possui a ilusão
de que as tarefas estão sendo executadas em paralelo.

O exemplo utilizado apenas ilustra a criação de duas threads,
uma por personagem, que não interagem.

No exercício,
o aluno vai precisar "sincronizar" as threads.
Eles provavelmente vão fazê-lo utilizando tempo.

## Exercícios adicionais
