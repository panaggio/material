---
track: Scratch
title: Condicional
---

## Exemplos

-   [Cachorro e gato controlado por usuário](/instrutores-scratch/exemplos/condicao-mover.sb).
-   [Primeiro jogo](/instrutores-scratch/exemplos/condicao-solucao.sb), essa é a
    solução do exercício proposto.
-   [Reimplementação do Rail Rush](/instrutores-scratch/exemplos/rail-rush.sb).

    Caso deseje alterar as imagens:
    [fundo](/img/scratch/rail-rush-palco.svg),
    [trem](/img/scratch/rail-rush-trem.svg) e
    [carro](/img/scratch/rail-rush-carro.svg).
</section>
    

## Computação desplugada

Comprar algumas caixas de bombons e colocá-los em um saco opaco. Organizar os
alunos em uma roda e fazer eles escreverem uma condição composta
(envolvendo o condicional E) para o bombom que eles querem[^*].
Passar o saco pelos alunos que
tiram um bombom, verificam se satisfaz a condição que escreveu e se tiver sido
satisfeita fica com o bombom.

[^*]: Não vale regras como

    -   `TRUE`
    -   "possui chocolate", "feito de chocolate", ...
    -   "nome é sonho de valsa", "nome é mundi", ...

## Comentários

Esse provavelmente é o tópico mais difícil de ensinar
mas também o que deve animá-los mais
pois depois desse eles vão poder fazer coisas mais úteis.

## Exercícios adicionais
