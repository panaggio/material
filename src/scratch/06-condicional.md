---
track: Scratch
title: Condicional
---

No exemplo anterior,
você colocou o cachorro correndo atrás do gato
e o gato tentando fugir.
Agora você vai alterar o exemplo anterior
para que uma pessoa possa controlar o cachorro
e outra o gato.

O cachorro irá responder às teclas `←`, `↑`, `↓` e `→`.
Para saber quando o usuário pressiona uma dessas teclas
você irá utilizar o bloco `tecla () pressionada?`
presente na palheta `Sensores`.

Para associar o deslocamento do objeto
com o pressionar de uma tecla
você irá utilizar o bloco `se ()`
presente na palheta `Controle`.

Depois adicione os blocos na aba Comandos do `objeto2`
e edite os valores presentes neles para ficar com

~~~
quando ⚑ clicado
vá para x: (150) y: (-100)
sempre
    se (tecla (←) pressionada?)
        mude x por (-10)
    se (tecla (↑) pressionada?)
        mude y por (10)
    se (tecla (↓) pressionada?)
        mude y por (-10)
    se (tecla (→) pressionada?)
        mude x por (10)
~~~

![Screenshot do `objeto2` com a aba `Comandos` preenchida.](/img/scratch/condicional-cachorro.jpg)

O gato irá responder de forma análoga ao cachorro
mas ao invés das teclas `←`, `↑`, `↓` e `→`
será utilizado `a`, `w`, `s` e `d`.

~~~
quando ⚑ clicado
vá para x: (-150) y: (100)
sempre
    se (tecla (a) pressionada?)
        mude x por (-10)
    se (tecla (w) pressionada?)
        mude y por (10)
    se (tecla (s) pressionada?)
        mude y por (-10)
    se (tecla (d) pressionada?)
        mude x por (10)
~~~

![Screenshot do `objeto1` com a aba `Comandos` preenchida.](/img/scratch/condicional-gato.jpg)

Agora que você terminou de escrever seu programa você pode testá-lo.

<section class="exercises">
#### Exercícios

#.  Seu primeiro jogo está quase concluído.
    Só falta adicionar um final quando o cachorro alcançar o gato.
    Adicione um fim ao seu programa utilizando os blocos:

    -   `pare tudo` da palheta `Controle`,
    -   `se ()` da palheta `Controle`, e
    -   `tocando em ()?` da palheta `Sensores`.

#.  [Rail Rush](https://marketplace.firefox.com/app/rail-rush?src=search)
    é um jogo para celular bem simples.
    Você precisa controlar o cruzamento de uma rodovia com uma estrada.
    Recrie esse jogo utilizando as imagens abaixo.

    ![Fundo do palco para recriação do Rail Rush.](/img/scratch/rail-rush-palco.png)

    ![Trem para recriação do Rail Rush. Modificado de <https://openclipart.org/detail/173654/train-pictogram>.](/img/scratch/rail-rush-trem.png)

    ![Carro para recriação do Rail Rush. Retirado de <https://openclipart.org/detail/190177/dark-blue-racing-car-top-view>.](/img/scratch/rail-rush-carro.png)
</section>
