---
track: Hardware
title: Comunicação entre componentes
---

## Exemplos

## Jogos de Hardware

Para auxiliar na compreensão do papel de cada parte do hardware e alguns detalhes, desenvolvemos alguns "jogos":

### Os componentes - V0 programa de soma com 2 números
*objetivo*: entender basicamente o funcionamento de um computador com cpu, teclado e monitor

*como funciona*: existem 3 alunos envolvidos, CPU, teclado e tela. A CPU possui 3 espaços, e só sabe realizar operações de soma e exibir na tela. O teclado insere os números e a tela exibe (a pedido da CPU).

```programa:
1 - repita para sempre:
2 -   ler teclado
3 -   guardar valor no primeiro espaço livre da CPU
4 -   ler teclado
5 -   guardar valor no segundo espaço livre da CPU
6 -   somar primeiro espaço livre com segundo espaço livre, e guardar resultado no terceiro espaço livre
7 -   repassar para a tela o terceiro espaço do processador
```

*modificações possíveis*:
-inserir novas operações (subtração por exemplo) no processador , e fazer um programa que as utilize
-modificar o programa para que ele exiba na tela os números digitados (e o caracter de +), durante a digitação ou após o cálculo
-desafio: como realizar uma multiplicação sem operação de multiplicação

### Os componentes - V1 programa de soma com 4 números e somente 3 posições no processador
*objetivo*: entender o funcionamento básico do computador , com cpu, teclado, monitor e memória.

*como funciona*: pode começar verificando como seria possível somar 4 números só com 3 posições (existe a possibilidade de fazer a soma cumulativa, pode-se trabalhar nessa linha, mas depois é necessário especificar que queremos ter todos os números no fim da operação). Depois da discussão, indique que será necessário um novo "participante", no caso a memória. A memória terá 4 posições livres. O processador agora também sabe como guardar coisas na memória, e ler coisas da memória. Executa-se o programa a seguir.

```programa:
1 - repita para sempre:
2 -   ler teclado
3 -   guardar valor no primeiro espaço da memória
4 -   ler teclado
5 -   guardar valor no segundo espaço da memória
6 -   ler teclado
7 -   guardar valor no quarto espaço da memória
8 -   ler teclado
9 -   guardar valor no quinto espaço da memória
10 -  ler valor da primeiro espaço da memória no primeiro espaço do processador
11 -  ler valor da segundo espaço da memória no primeiro espaço do processador
12 -   somar primeiro espaço livre com segundo espaço livre, e guardar resultado no terceiro espaço livre
13 -   mover terceiro espaço livre para primeiro espaço livre
14 -   ler valor da terceiro espaço da memória no segundo espaço do processador
15 -   somar primeiro espaço livre com segundo espaço livre, e guardar resultado no terceiro espaço livre
16 -   mover terceiro espaço livre para primeiro espaço livre
17 -   ler valor da quarto espaço da memória no segundo espaço do processador
18 -   somar primeiro espaço livre com segundo espaço livre, e guardar resultado no terceiro espaço livre
19 -  exibir na tela terceiro espaço do processor
```

*modificações possíveis*:
-exibir todos os números e o resultado (pode-se considerar possibilidade de exibir direto da memória ou só do processador)
-modificar programa para usar laço
-possibilidade deles criarem os próprios programas


### Os componentes - V2.1 programa de calculadora 4 operações (usando números de qualquer tamanho)

*objetivo*:  entender o funcionamento de um computador digital -

*como funciona *: escrever o "programa" em cartões postit  -
designar alunos para serem o "teclado" (anota um número num postit")-  o "barramento" -
caminha com dados entre teclado, tela e CPU, a "CPU" - anota, copia, despacha os dados,
faz as operações.  (opcionalemtne mais um aluno para a "Unidade lógica e aritmetica da CPU" que faz as operações),
1 aluno para a "tela" - transcreve os números para o usuário final.

O programa:

```
Começar na CPU com 3 espaços em papel para anotar coisas:
o número "principal"
o "outro operando"
e a "operação"


1- aguardar uma tecla do teclado.
2- se não for um numero vá para o cartao 17
3- guardar o número no espaço "numero principal"
4- enviar essa cópia para a tela
5- se não for uma operação vá para o cartao 17
6- guarde a operação no espaço "operação"
7- copie o número principal e ponha a copia no espaço "outro operando"
8- apague o número principal e deixe 0
9- aguardar uma tecla do teclado
10- se não for um número, vá para o 17.
11- guardar o número no espaço "numero principal"
12- enviar essa cópia para a tela
13 - se não for "=" vá para o cartão 17
14- faça a operação com o numero principal e o outro operando, guarde o resultado no "numero principal"
15- copie o numero principal e mande para a tela
16- pare o programa.
17- escreva "erro" num post it
18- mande para a tela
19- volte para o cartao 1
```





### Os componentes - V2.2 programa de calculadora 4 operações (lendo digito à digito)
*objetivo*:  entender o funcionamento de um computador digital -

*como funciona *: escrever o "programa" em cartões postit  -
designar alunos para serem o "teclado" (anota um número num postit")-  o "barramento" -
caminha com dados entre teclado, tela e CPU, a "CPU" - anota, copia, despacha os dados,
faz as operações.  (opcionalemtne mais um aluno para a "Unidade lógica e aritmetica da CPU" que faz as operações),
1 aluno para a "tela" - transcreve os números para o usuário final.

O programa:

```
Começar na CPU com 3 espaços em papel para anotar coisas:
o número "principal"
o "outro operando"
e a "operação"


1- aguardar uma tecla do teclado.
2- se não for um numero vá para o cartao 7
3-     acrescentar o digito teclado ao espaço "numero principal"
4-     copiar todo o numero principal  (usar um post-it a parte)
5-     enviar essa cópia para a tela
6-     voltar para o cartão 1
7- se não for uma operação vá para o cartao 21
8- guarde a operação no espaço "operação"
8- copie o número principal e ponha a copia no espaço "outro operando"
9- apague o número principale  deixe 0
10- mande "0" para a tela
11- aguardar uma tecla do teclado
12- se não for um número, vá para o 17.
13-     acrescentar o digito teclado ao espaço "numero principal"
14-     copiar todo o numero principal  (usar um post-it a parte)
15-     enviar essa cópia para a tela
16-     voltar para o cartão 11
17 - se não for "=" vá para o cartão 21
18- faça a operação com o numero principal e o outro numero, guarde o resultado no "numero principal"
19-copie o numero principal e mande para a tela
20- pare o programa.
21- escreva "erro" num post it
22- mande para a tela
23- volte para o cartao 1
```


### Diferença de Unidades Kilo, Mega, Giga, Tera

Fazer algumas "placas" com quantidade de dados escritos (300 KB, 400 MB, 2 GB), e pedir pra eles ordenarem.
É possível também inserir valores em bits e bytes, para fortalecer a diferença entre os dois.




### Os componentes - V1

*Objetivo*: Entender qual a função do processador e do HD, e uma noção do quê é um progrma de computador.

*Como funciona*: Precisa de 2 alunos, 1 será o processador e  outro o HD. O HD recebe uma quantidade de caixas com chave, cada uma com o nome de uma letra (A,B, C...), dentro de cada uma delas existe um número.
O processador recebe uma lista de ações que precisa executar (operações matemáticas simples), baseados nas "caixas" do HD, por exemplo A + B.
Para cada ação, o processador terá que pedir para o HD o valor da caixa e a partir dele fazer a conta. O HD leva um tempo para abrir a caixa e passar o valor para a CPU.
A CPU tem lugares (2) para guardar os valores, e ela só pode realizar operações caso os valores estejam nesses "lugares".

*Exemplo de ações para o processador*:
A + B
C - D
A + B - C


### Os componentes - V2

*Objetivo*: Inserir a memória, para compreensão do seu papel.


*Como funciona*: Precisa de 3 alunos, agora 1 deles será a memória. O HD continua igual, a memória tem locais para guardar os valores (locais numerados), que não precisam ser abertos.
O processador quando precisar de um valor, pede para memória, se ela não tiver pede para o HD.
É possível fazer um comparativo do V1 e V2, marcando tempo, e ver qual fica mais rápido

### Os componentes - V3

*Objetivo*: Inserir vídeo e teclado para compreensão do papel da entrada e saída.

### O programa

No final dos jogos dos componentes, é possível fazer uma análise do quê é a "lista de ações para o processador", que no caso faz o papel do programa binário. A partir disso é possível explicar o código fonte e a compilação.
É possível pensar em uma dinâmica onde se tem o código fonte, e se insere um "tradutor" antes de chegar ao processador o quê ele deve fazer.


## Comentários

## Exercícios adicionais
