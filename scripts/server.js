/* jshint node: true */

var express = require('express');
var serveStatic = require('serve-static');

var server = express();

server.use(serveStatic(__dirname + '/../dist/web'));
server.use('/css', serveStatic(__dirname + '/../src/css'));
server.use('/img', serveStatic(__dirname + '/../src/img'));
server.use(
  '/instrutores-scratch/exemplos/',
  serveStatic(__dirname + '/../src/instrutores-scratch/exemplos/')
);

module.exports = server;
